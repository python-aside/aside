# Contributing

When contributing to this repository, please first discuss the change you wish
to make via issue, email, or any other method with the owners of this repository
before making a change.

## Contributing Process

1. Install `python3.6`. You can install it via your system package manager or by
   using `pyenv`. Our CI/CD uses `python3.6` and we officially support python
   versions 3.6 and later.

2. Run the `scripts/venv/create.sh` script to create a virtual environment and
   install all the development dependencies.

3. Verify that you can successfully run the `scripts/dev/lint_test.sh` script
   without errors on the current `master`.

4. Create a `dev/{your_name}/{your_contribution}` branch (recommended) from the
   current `master`. For example, `dev/ruro/fix-task-colors`.

5. Hack away. Please:
    - make small, descriptively named, logically grouped commits (you can
      interactively rebase them before pushing)
    - minimize meaningless changes in your commits (whitespace changes, changes
      that revert your previous commits)
    - don't change files outside of `src` and `tests` directories (unless you
      discussed these changes with us in advance)
    - if your contribution changes `setup.cfg` or any files in `requirements/`,
      carefully read [Requirements](requirements)
    - if your contribution adds any binary or non-human-readable files,
      carefully read and follow the instructions in the
      [Git LFS](#git-lfs) section
    - document your code (docstrings, comments)
    - write unit tests for your code (unless you are contributing pure UI code)
    - regularly run `scripts/dev/isort_black.sh` (at least once before each
      commit)
    - regularly run `scripts/dev/lint_test.sh` (at least once before each push)

   Your contribution might be rejected if you don't follow these requirements.
   The last 4 bullet points are checked and enforced by CI/CD.

6. Verify, that the `scripts/dev/lint_test.sh` still runs without errors.
   Consider fixing any new warnings, that you might have introduced.

7. Check if `master` was updated while you were coding. If it was, perform a
   `git pull --rebase` and repeat step 6.

8. Push your changes to `dev/{your_name}/{your_contribution}` and open a merge
   request to `master`. Carefully fill in the merge request template and wait
   for your code to be reviewed and for the pipelines to complete. If you did
   everything correctly, you should see 7 green badges appear in the **Badges**
   section of your pull request after the pipeline finishes running.

## Scripts

Most scripts hide the output of the running commands unless an error or warning
is encountered. You can force the scripts to always show the command output by
setting the `VERBOSE` environment variable.

Most scripts that generate temporary files, place them into the `.out`
directory.

Many scripts also require the virtual environment to work. You can create the
virtual environment by running `scripts/venv/create.sh`. After that you don't
need to activate the virtual environment, the scripts will activate it
automatically.

### `venv` scripts

- `create.sh` initialize the virtual environment and synchronize it with
  `requirements/_txt`
- `requirements_compile.sh` compile `requirements/_txt`
  (see [Requirements](requirements))

### `dev` scripts

- Basic operations.
    - `clean_all.sh` remove all generated and temporary files
    - `isort_black.sh` reformat all python code according to project code style
    - `lint_test.sh` run all validation checks used in the project
      (see [Validation](#validation))
    - `build_package.sh` build and validate the python package
    - `serve_docs.sh` continuously build and serve the documentation on
      localhost
- Resources & SVGs.
    - `generate_resources.sh` extract font glyphs and build the resource files
    - `svg` python scripts for svg generation
      (run `python scripts/dev/svg --help` to list commands)

### `cicd` scripts

Although you can manually run some scripts in the `cicd` directory, you probably
shouldn't as the `dev` scripts provide better convenience wrappers of the `cicd`
scripts in most cases.

## Validation

This project uses the following tools to validate the code quality and
correctness:

- Code Style.
    - `black`
    - `isort`
    - `pylint`
- Test & Coverage.
    - `coverage` (via `pytest-cov`)
    - `pytest`
    - `twine` (package validation)
- Documentation.
    - `pydocstyle`
    - `sphinx`

Ideally, you shouldn't need to run these tools manually, as they can
automatically be invoked with all the **recommended™** options by using the
[`dev` scripts](#dev-scripts).

## Git LFS

Some files in this repository (mainly images) are stored using
[`git lfs`](https://git-lfs.github.com/).

If you cloned the repository and some of the files look like this:

```
version https://git-lfs.github.com/spec/v1
oid sha256:0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef
size 666
```

Then you didn't install or didn't enable `git lfs` correctly.

You should `git lfs track` any binary or large non-human-readable text files
**before** committing them. If you have accidentally committed such files
without `git lfs track`ing them, you **will** need to rewrite your commit
history to fix that.

Please, **DO NOT PUSH** such commits to the repository even if you don't plan to
open a merge request with them.
