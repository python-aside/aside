```{include} ../README.md
```

```{toctree}
:hidden:

Overview <self>
```

```{todo}
Add high level overview of this documentation and of the package itself.
```

```{toctree}
:caption: User Documentation
:glob:
:hidden:
:maxdepth: 2
:numbered:

usrdoc/*
```

```{toctree}
:caption: Developer Documentation
:hidden:
:maxdepth: 4
:titlesonly:

contributing
devdoc/aside
genindex
```
