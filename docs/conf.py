import os

import aside.version

# region General
root = aside

project = root.__name__
author = f"{project} developers"
copyright = f"2021, {author}"
release = root.version.version
version = root.version.base_version

resource_prefix = "../resources/"
if "READTHEDOCS" in os.environ:
    # READTHEDOCS runs sphinx directly from the `docs` directory.
    doc_root = "."
else:
    doc_root = "docs"

add_module_names = False
default_role = "py:obj"
extensions = []
modindex_common_prefix = [project + "."]
nitpicky = True
nitpick_ignore_regex = [
    ("py:class", r"Pattern\[.*\]"),
    ("py:class", r"typing_extensions.*"),
    ("py:class", r"importlib_resources.*"),
]
python_use_unqualified_type_names = True
# endregion

# region AutoDoc
extensions.append("sphinx.ext.autodoc")

autoclass_content = "class"
# ToDo: replace autodoc_class_signature with "separated" when
#       [this PR](https://github.com/sphinx-doc/sphinx/pull/9472)
#       gets merged/released.
autodoc_class_signature = "mixed"
autodoc_default_options = {
    "member-order": "bysource",
    "undoc-members": True,
    "ignore-module-all": True,
}
autodoc_type_aliases = {
    "Attribute": "attr.Attribute",
}
# endregion

# region InterSphinx & Qt Docs
extensions.append("sphinx.ext.intersphinx")
extensions.append("sphinx_qt_documentation")

intersphinx_mapping = {
    "PyQt5": ("https://riverbankcomputing.com/static/Docs/PyQt5/", None),
    "attrs": ("https://www.attrs.org/en/stable/", None),
    "python": ("https://docs.python.org/3/", None),
}
for pkg in [
    "importlib-resources",
    "setuptools",
]:
    pkg_ref = pkg.replace("-", "_")
    rtd_url = f"https://{pkg}.readthedocs.io/en/stable/"
    intersphinx_mapping[pkg_ref] = (rtd_url, None)
# endregion

# region Napoleon
extensions.append("sphinx.ext.napoleon")

napoleon_google_docstring = True
napoleon_include_init_with_doc = True
napoleon_numpy_docstring = False
# endregion

# region
extensions.append("sphinx.ext.todo")

todo_include_todos = True
# endregion

# region ApiDoc
extensions.append("sphinxcontrib.apidoc")

apidoc_extra_args = ["--force", f"--templatedir={doc_root}/templates/apidoc"]
(apidoc_module_dir,) = root.__path__
apidoc_output_dir = "devdoc"

apidoc_module_first = True
apidoc_separate_modules = True
apidoc_toc_file = False
# endregion

# region ViewCode & MyST Parser
extensions.append("sphinx.ext.viewcode")
extensions.append("myst_parser")
# endregion

# region ReadTheDocs
# ToDo: Remove this if/when
#       [this issue](https://github.com/readthedocs/readthedocs.org/issues/1846)
#       is fixed.
if "READTHEDOCS" in os.environ:
    if not os.path.exists("./git-lfs"):
        os.system(
            "wget https://github.com/git-lfs/git-lfs/releases/download"
            "/v2.13.3/git-lfs-linux-amd64-v2.13.3.tar.gz"
        )
        os.system("tar xvfz git-lfs-linux-amd64-v2.13.3.tar.gz git-lfs")
    os.system("./git-lfs install")
    os.system("./git-lfs fetch")
    os.system("./git-lfs checkout")
    # first git-lfs checkout will fail with
    # `the remote end hung up unexpectedly`
    # so we just do it twice
    os.system("./git-lfs checkout")
# endregion

# region HTML Theme
extensions.append("sphinx_rtd_theme")

html_title = project
html_last_updated_fmt = "%d/%m/%Y"
html_favicon = f"{resource_prefix}favicon.ico"
html_logo = f"{resource_prefix}logo.svg"

html_theme = "sphinx_rtd_theme"
html_theme_options = {
    "display_version": True,
    "logo_only": True,
}
# endregion
