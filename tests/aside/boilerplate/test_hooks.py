import random
from contextlib import redirect_stdout
from io import StringIO
from string import ascii_letters, digits

import pytest

from aside import config, theme
from aside.boilerplate import hooks


def test_config_import():
    """`aside.config` must export a `config` variable."""
    assert hasattr(config, "config")


def test_theme_import():
    """`aside.theme` must export a `theme` variable."""
    assert hasattr(theme, "theme")


def random_string(length=16):
    """Generate a random string of letters and digits of specified length."""
    return "".join(random.choice(ascii_letters + digits) for _ in range(length))


def test_load_hooks_order(tmp_path):
    """Check that hooks are loaded in lexicographic order."""
    hook_dir = tmp_path / "aside"
    hook_dir.mkdir()

    hook_files = sorted(
        [
            hook_dir / f"{random_string()}.py",
            hook_dir / f"{random_string()}.py",
        ]
    )

    markers = []
    for hook in hook_files:
        marker = random_string()
        markers.append(marker)
        hook.write_text(f"print('{marker}')")

    stdout = StringIO()

    config.config.verbose = False
    with redirect_stdout(stdout):
        hooks.load_user_hooks([tmp_path])

    config.config.verbose = True
    with redirect_stdout(stdout):
        hooks.load_user_hooks([tmp_path])

    expected = (
        "\n".join(
            [
                "\n".join(markers),
                f"Searching for hooks in {hook_dir}.",
                "\n".join(
                    f"Loading hooks from {hook_file}.\n{mark}"
                    for hook_file, mark in zip(hook_files, markers)
                ),
            ]
        )
        + "\n"
    )

    assert stdout.getvalue() == expected


boolean_values = [False, True]
color_values = ["rgb(255, 0, 0)", "#ff0000"]
alpha_values = [0.0, 0.5, 1.0]


@pytest.mark.parametrize(
    "module, attribute, value",
    [
        (config, attribute, value)
        for attribute, values in {
            "verbose": boolean_values,
        }.items()
        for value in values
    ]
    + [
        (theme, attribute, value)
        for attribute, values in {
            "background_color": color_values,
            "icon_circle_color": color_values,
            "icon_circle_hi_color": color_values,
            "icon_content_color": color_values,
            "icon_text_color": color_values,
            "icon_shadow_alpha": alpha_values,
        }.items()
        for value in values
    ],
)
def test_load_hooks_register(module, attribute, value, tmp_path):
    """Check that attribute override works from user hooks."""
    kind = module.__name__.split(".")[-1]

    hook_dir = tmp_path / "aside"
    hook_dir.mkdir()

    hook = hook_dir / "hook.py"
    hook.write_text(
        f"from aside.{kind} import register_{kind}\n"
        f"@register_{kind}\n"
        "class MyClass:\n"
        f"    {attribute} = {value!r}\n"
    )

    config.config.verbose = False
    hooks.load_user_hooks([tmp_path])
    target = getattr(module, kind)

    assert getattr(target, attribute) == value


@pytest.mark.parametrize(
    "contents",
    [
        (
            # Just raise some Exception
            "raise ValueError()\n"
        ),
        (
            # Syntax error
            "with = 'This is a syntax error'\n"
        ),
        (
            # Inifinite recursion
            "def infinite_recursion(): infinite_recursion()\n"
            "infinite_recursion()\n"
        ),
        (
            # Inifinite recursion into load_user_hooks
            "from aside.hooks import load_user_hooks\n"
            "load_user_hooks([__name__.rsplit('/', 2)[0]])\n"
        ),
        (
            # Setting an unknown attribute
            "from aside.config import register_config\n"
            "@register_config\n"
            "class MyClass:\n"
            "    this_attribute_doesnt_exist = 'somevalue'\n"
        ),
    ],
)
def test_load_raising_hooks(contents, tmp_path):
    """Check that invalid hooks cause `RuntimeError`s."""
    hook_dir = tmp_path / "aside"
    hook_dir.mkdir()

    hook = hook_dir / "hook.py"
    hook.write_text(contents)

    with pytest.raises(RuntimeError):
        hooks.load_user_hooks([tmp_path])


def test_get_user_hook_dir(tmp_path):
    """Check that `hooks.get_user_hook_dir` initializes the config."""
    default_path = hooks.get_user_hook_dir(init_missing=False, hook_dir=tmp_path)
    assert default_path == (tmp_path / "aside")
    assert not default_path.exists()

    default_path = hooks.get_user_hook_dir(hook_dir=tmp_path)
    assert default_path == (tmp_path / "aside")
    assert default_path.exists()
    assert default_path.is_dir()

    default_config = default_path / "config.py"
    assert default_config.exists()
    assert default_config.is_file()
