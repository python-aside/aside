import pytest
from attr.exceptions import FrozenAttributeError

from aside.boilerplate import attrs
from aside.boilerplate.observable import (
    Event,
    EventType,
    Observable,
    ObservableCollection,
    observable,
    uuid_attrib,
)


def test_observable_event_emittance():
    """Test that Observable objects can e subscribed to and can invoke callbacks."""

    @observable
    @attrs
    class SomeClass(Observable):
        some_number: int = 0
        some_string: str = ""

    times_triggered = []

    def observer(event: Event) -> None:
        times_triggered.append(1)
        assert event.attr_name == "some_number"
        assert event.obj is instance

    instance = SomeClass()
    instance.subscribe(observer, "some_number")
    assert len(times_triggered) == 0
    instance.some_number += 1
    assert len(times_triggered) == 1
    assert instance.some_number == 1
    instance.some_string = "New string!"
    assert len(times_triggered) == 1
    assert instance.some_string == "New string!"


def test_observable_collection_base_methods():
    """Test collection base methods."""

    @observable
    @attrs
    class SomeObservableClass(Observable):
        uuid: str
        data: str

    def observer_changed_collection(event: Event):
        if event.event_type == EventType.CHANGE:
            assert event.get_nested_object() is copy_element
        if event.event_type == EventType.REASSIGN:
            assert event.get_nested_object() is element

    collection = ObservableCollection()
    collection.subscribe(
        observer_changed_collection, event_types=[EventType.CHANGE, EventType.REASSIGN]
    )
    element = SomeObservableClass(uuid="same uuid", data="Text for first element!")
    collection.add(element)
    collection.add(element)  # this should raise REASSIGN event
    assert len(collection) == 1
    assert element in collection
    assert collection[element.uuid] == element
    copy_element = SomeObservableClass(
        uuid="same uuid", data="Text for another element!"
    )
    collection.add(copy_element)  # this should raise CHANGE event
    assert len(collection) == 1
    assert element in collection  # this behaviour is weird, but correct as uuid match
    assert collection[copy_element.uuid] == copy_element
    new_element = SomeObservableClass(
        uuid="new uuid", data="Text for yet another element!"
    )
    collection.add(new_element)
    assert len(collection) == 2
    matched_data = filter(lambda x: "Text" in collection[x].data, collection)
    assert len([*matched_data]) == 2
    collection.discard(copy_element)
    assert len(collection) == 1
    assert element not in collection
    collection.discard(element)
    assert len(collection) == 1


def test_observable_collection_no_uuid_property():
    """Test that collection doesn't work with Observable classes without UUID."""

    @observable
    @attrs
    class SomeBadClass(Observable):
        data: str = ""

    collection = ObservableCollection()
    with pytest.raises(AttributeError):
        collection.add(SomeBadClass())

    with pytest.raises(AttributeError):
        collection.discard(SomeBadClass())

    with pytest.raises(AttributeError):
        _ = SomeBadClass() in collection

    with pytest.raises(KeyError):
        _ = collection["Doesn't exist"]


def test_observable_collection_events():
    """Test if collection correctly handles events from children."""

    @observable
    @attrs
    class SomeSimpleObservableClass(Observable):
        uuid: str
        data: str

    times_triggered = []

    def observer(event: Event) -> None:
        times_triggered.append(1)
        split_path = event.attr_name.split("/")
        assert event.obj[split_path[0]][split_path[1]] == "New text!"

    collection = ObservableCollection()
    element = SomeSimpleObservableClass(uuid="uuid", data="Text for first element!")
    element2 = SomeSimpleObservableClass(uuid="uuid2", data="Text for second element!")
    collection.add(element)
    collection.add(element2)
    collection.subscribe(observer)
    for elem in collection:
        collection[elem].data = "New text!"
    assert len(times_triggered) == 2
    assert collection[element.uuid].data == "New text!"
    assert collection[element2.uuid].data == "New text!"


def test_observable_nested_observables():
    """Test if events propagate correctly from nested children to parents."""

    @observable
    @attrs
    class SomeSimpleObservableClass(Observable):
        data: str

    @observable
    @attrs
    class ComplexObservableClass(Observable):
        uuid: str
        observable_attr: SomeSimpleObservableClass

    triggered_functions = []

    def observer_for_observable(event: Event) -> None:
        triggered_functions.append(1)
        assert isinstance(event.get_nested_object(level=2), SomeSimpleObservableClass)

    def observer_for_data(event: Event) -> None:
        triggered_functions.append(2)
        assert event.get_nested_object() == "New text!"

    def observer_for_all(event: Event) -> None:
        triggered_functions.append(3)
        assert event.obj is collection

    collection = ObservableCollection()
    simple_object = SomeSimpleObservableClass(data="Old text")
    complex_object = ComplexObservableClass(uuid="uuid", observable_attr=simple_object)
    collection.subscribe(observer_for_observable, ".*/observable_attr/.*", order=0)
    collection.subscribe(
        observer_for_data, ".*/.*/data", order=10, event_types=[EventType.CHANGE]
    )
    collection.subscribe(
        observer_for_all, event_types={EventType.ADD, EventType.DISCARD}
    )
    collection.add(complex_object)
    assert len(triggered_functions) == 1
    for elem in collection:
        collection[elem].observable_attr.data = "New text!"
    assert len(triggered_functions) == 3
    for elem in collection:
        collection[elem].observable_attr.data = "New text!"
    assert len(triggered_functions) == 4

    assert triggered_functions == [3, 1, 2, 1]
    assert collection[complex_object.uuid].observable_attr is simple_object
    assert collection[complex_object.uuid].observable_attr.data == "New text!"


def test_observable_discarded():
    """Test correctness of discarding observable item from observable collection."""

    @observable
    @attrs
    class SomeSimpleObservableClass(Observable):
        data: str

    @observable
    @attrs
    class ComplexObservableClass(Observable):
        uuid: str
        observable_attr: SomeSimpleObservableClass

    triggered_functions = []

    def observer_complex(event: Event) -> None:
        triggered_functions.append(1)
        assert event.get_nested_object() is simple2

    def observer_simple(event: Event) -> None:
        triggered_functions.append(2)
        assert event.get_nested_object() == "Even newer text!"

    simple1 = SomeSimpleObservableClass(data="Old text")
    simple2 = SomeSimpleObservableClass(data="Also old text")
    comp = ComplexObservableClass(uuid="123", observable_attr=simple1)
    simple1.data = "new text"
    assert len(triggered_functions) == 0
    comp.subscribe(observer_complex)
    simple1.subscribe(observer_simple)
    # this should reset ownership of simple1, but leave intact it's subscribers
    comp.observable_attr = simple2
    assert len(triggered_functions) == 1
    # this shouldn't emit an event for `comp`, but still
    simple1.data = "Even newer text!"
    assert len(triggered_functions) == 2
    assert triggered_functions == [1, 2]


def test_observable_multiple_owners():
    """Test that observable can't be assigned to multiple observable owners."""

    @observable
    @attrs
    class SomeSimpleObservableClass(Observable):
        data: str

    @observable
    @attrs
    class ComplexObservableClass(Observable):
        uuid: str
        observable_attr: SomeSimpleObservableClass

    simple1 = SomeSimpleObservableClass(data="Old text")
    comp = ComplexObservableClass(uuid="123", observable_attr=simple1)
    comp2 = None
    with pytest.raises(AttributeError):
        comp2 = ComplexObservableClass(uuid="123", observable_attr=simple1)

    del comp
    del comp2


def test_observable_uuid_attrib():
    """Test that uuid_attrib creates frozen attribute."""

    @observable
    @attrs
    class ObservableClass(Observable):
        uuid: str = uuid_attrib()
        data: str = ""

    obj = ObservableClass()
    assert isinstance(obj, Observable)
    assert isinstance(obj.uuid, str)
    with pytest.raises(FrozenAttributeError):
        obj.uuid = "My uuid!"


def test_observable_iterable_init_and_repr():
    """Test observable init and repr correctness."""

    @observable
    @attrs
    class ObservableClass(Observable):
        uuid: str = uuid_attrib()
        data: str = ""

    list_of_observables = [ObservableClass(uuid=str(i)) for i in range(4)]
    collection = ObservableCollection(list_of_observables)
    assert len(collection) == 4
    exp_repr = (
        "ObservableCollection("
        "[ObservableClass(uuid='0', data=''), "
        "ObservableClass(uuid='1', data=''), "
        "ObservableClass(uuid='2', data=''), "
        "ObservableClass(uuid='3', data='')])"
    )
    assert repr(collection) == exp_repr


def test_observable_dropping_observers():
    """Test that observable drops observers and they are not called anymore."""

    @observable
    @attrs
    class SomeSimpleObservableClass(Observable):
        data: str

    triggered_functions = []

    def observer_1(event: Event) -> None:
        triggered_functions.append(1)
        assert event.get_nested_object() == "First set!"

    def observer_2(event: Event) -> None:
        triggered_functions.append(2)
        assert event.get_nested_object() == "Second set!"

    simple1 = SomeSimpleObservableClass(data="Old text")
    simple1.subscribe(observer_1)
    simple1.data = "First set!"
    assert triggered_functions == [1]
    simple1.drop_observers()
    simple1.subscribe(observer_2)
    simple1.data = "Second set!"
    assert triggered_functions == [1, 2]


def test_observable_nonexistent_attr():
    """Observable object must raise error on to get invalid attribute."""

    @observable
    @attrs
    class SomeSimpleObservableClass(Observable):
        data: str

    simple1 = SomeSimpleObservableClass(data="Old text")
    with pytest.raises(AttributeError):
        _ = simple1["attr_not_in_class"]
