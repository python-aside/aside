from typing import Dict

import pytest
from attr import Factory
from attrs_strict import AttributeTypeError

from aside.boilerplate import attrs


def test_attrs_class_defaults():
    """`attrs` must also populate the default values in classes."""

    @attrs()
    class SomeClass:
        some_attr: int = 123
        other_attr: str

    assert SomeClass.some_attr == 123
    assert not hasattr(SomeClass, "other_attr")


def test_attrs_invalid_init():
    """`attrs` must raise errors on invalid arguments in `__init__`."""

    @attrs
    class SomeClass:
        val: Dict[str, int]

    with pytest.raises(TypeError):
        SomeClass()

    with pytest.raises(AttributeTypeError):
        SomeClass(val="")

    with pytest.raises(AttributeTypeError):
        SomeClass(val={"foo": "bar"})

    with pytest.raises(AttributeTypeError):
        SomeClass(val={123: 321})


def test_attrs_invalid_setattr():
    """`attrs` must raise errors on invalid attribute assignment."""

    @attrs
    class SomeClass:
        val: Dict[str, int] = Factory(dict)

    with pytest.raises(AttributeTypeError):
        SomeClass().val = ""

    with pytest.raises(AttributeTypeError):
        SomeClass().val = {"foo": "bar"}

    with pytest.raises(AttributeTypeError):
        SomeClass().val = {123: 321}
