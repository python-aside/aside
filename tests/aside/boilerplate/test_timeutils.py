from datetime import datetime, timedelta

import pytest

from aside.boilerplate import timeutils


@pytest.mark.parametrize(
    "relative, delta",
    [
        ("in 1 minute", timedelta(minutes=1)),
        ("in 1 hour", timedelta(hours=1)),
        ("in 1 day", timedelta(days=1)),
        ("tomorrow", timedelta(days=1)),
        ("next week", timedelta(days=7)),
    ],
)
def test_rel_to_abs(relative, delta):
    """Test that ``timeutils.relative_to_absolute`` gives expected results."""
    now = datetime.now().astimezone()
    absolute = timeutils.relative_to_absolute(relative)
    assert abs((absolute - now).total_seconds() - delta.total_seconds()) < 30.0


@pytest.mark.parametrize(
    "delta",
    [timedelta(minutes=1), timedelta(hours=1)],
)
def test_abs_to_rel(delta):
    """Test that ``timeutils.absolute_to_relative`` gives expected results."""
    now = datetime.now().astimezone()
    relative = timeutils.absolute_to_relative(now + delta)
    absolute = timeutils.relative_to_absolute(relative)
    assert abs((absolute - now).total_seconds() - delta.total_seconds()) < 30.0
