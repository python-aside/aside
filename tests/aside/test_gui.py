from datetime import datetime, timedelta

import pytest
from PyQt5.QtCore import QAbstractAnimation, Qt
from PyQt5.QtGui import QDesktopServices

from aside import gui, models
from aside.gui.queue import Queue
from aside.gui.task import Task


def test_get_icon(qtbot):
    """`get_icon` must be able to load a valid svg icon."""
    ico1 = gui.common.get_icon("icon")
    ico2 = gui.common.get_icon("icon")
    # Ensure that the icons are cached
    assert ico1 is ico2


@pytest.mark.parametrize(
    "widget",
    [
        (gui.task.Task, models.models.Task()),
        (gui.queue.QueueHeader, models.models.Queue()),
        (gui.queue.Queue, models.models.Queue()),
        (gui.main_window.AsideWindow, models.models.QueueManager()),
    ],
    ids=lambda w: w[0].__name__,
)
def test_widget(qtbot, widget):
    """All the GUI widget must be initializable without errors."""
    widget, *args = widget
    widget_inst = widget(*args)
    qtbot.add_widget(widget_inst)
    widget_inst.show()
    qtbot.wait_exposed(widget_inst)


def click_and_wait(qtbot, widget, button=Qt.LeftButton):
    """Click on the button and wait for all the events to complete."""
    wait = qtbot.wait_signal(widget.clicked)
    qtbot.mouseClick(widget, button)
    wait.wait()


def wait_animations_finished(qtbot, widget):
    """Wait until all the children animations of this widget are not Running."""
    qtbot.wait_until(
        lambda: all(
            animation.state() != QAbstractAnimation.Running
            for animation in widget.findChildren(QAbstractAnimation)
        )
    )


def enter_with_focus(qtbot, widget, text):
    """Focus on the widget, enter text into it and then unfocus the widget."""
    widget.setFocus()
    qtbot.wait_until(lambda: widget.hasFocus())
    # Ctrl+A, Backspace
    qtbot.keyClick(widget, "a", Qt.ControlModifier)
    qtbot.keyClick(widget, Qt.Key_Backspace)
    # Enter target text
    qtbot.keyClicks(widget, text)
    widget.parentWidget().setFocus()
    qtbot.wait_until(lambda: not widget.hasFocus())


def test_open_stuff(monkeypatch, qtbot, tmp_path):
    """Clicking on settings and info buttons must open the hook_dir/docs."""
    calls = []

    def mock_open_url(url):
        calls.append(url.url())
        return True

    monkeypatch.setenv("XDG_CONFIG_HOME", str(tmp_path))
    monkeypatch.setattr(QDesktopServices, "openUrl", mock_open_url)

    widget = gui.main_window.AsideWindow(models.models.queue_manager)
    qtbot.add_widget(widget)
    widget.show()

    qtbot.wait_exposed(widget)
    for button in [widget.settings, widget.info]:
        click_and_wait(qtbot, button)

    assert calls == [
        str(tmp_path / "aside"),
        "https://aside.rtfd.io/en/stable",
    ]


def test_gui_end_to_end(qtbot):
    """Updating the GUI must update the model and vice versa."""
    root_mod = models.models.QueueManager()
    root_gui = gui.main_window.AsideWindow(root_mod)

    # Prepare the GUI
    qtbot.add_widget(root_gui)
    root_gui.show()
    qtbot.wait_exposed(root_gui)
    assert root_gui.model is root_mod

    # Add a queue by clicking the add queue button
    assert len(root_mod.queues) == len(root_gui.findChildren(Queue)) == 0
    click_and_wait(qtbot, root_gui.add_queue)
    (queue_mod,) = root_mod.queues.values()
    (queue_gui,) = root_gui.findChildren(Queue)
    assert queue_gui.model is queue_mod
    assert queue_gui.header.model is queue_mod

    # Add a task by clicking the add task button
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 0
    click_and_wait(qtbot, queue_gui.header.add_task)
    (task_mod,) = queue_mod.tasks.values()
    (task_gui,) = queue_gui.findChildren(gui.task.Task)
    assert task_gui.model is task_mod

    # Change the title of the queue
    title1 = "A Queue Title"
    enter_with_focus(qtbot, queue_gui.header.name, title1)  # from GUI
    assert len(root_mod.queues) == len(root_gui.findChildren(Queue)) == 1
    assert queue_gui.model is queue_mod
    assert queue_gui.header.model is queue_mod
    assert queue_mod.name == queue_gui.header.name.text() == title1

    # Change it through the model
    title2 = "A Different Queue Title"
    queue_mod.name = title2  # from Model
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 1
    assert queue_gui.model is queue_mod
    assert queue_mod.name == queue_gui.header.name.text() == title2

    # Change the title of the task
    title1 = "A Task Title"
    enter_with_focus(qtbot, task_gui.description, title1)  # from GUI
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 1
    assert task_gui.model is task_mod
    assert task_mod.text == task_gui.description.text() == title1

    # Change it through the model
    title2 = "A Different Task Title"
    task_mod.text = title2  # from Model
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 1
    assert task_gui.model is task_mod
    assert task_mod.text == task_gui.description.text() == title2

    # Change the deadline of the task
    tomorrow = datetime.now().astimezone() + timedelta(days=1, seconds=30)
    enter_with_focus(qtbot, task_gui.time, "in 1 day 30 seconds")  # from GUI
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 1
    assert task_gui.model is task_mod
    assert abs((task_mod.deadline - tomorrow).total_seconds()) < 30.0
    assert task_gui.time.text() == "in a day"

    # Try changing it to an invalid value
    enter_with_focus(qtbot, task_gui.time, "not a valid time")  # from GUI
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 1
    assert task_gui.model is task_mod
    assert abs((task_mod.deadline - tomorrow).total_seconds()) < 30.0
    assert task_gui.time.text() == "in a day"

    # Change it through the model
    now = datetime.now().astimezone()
    task_mod.deadline = now  # from Model
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 1
    assert task_gui.model is task_mod
    assert abs((task_mod.deadline - now).total_seconds()) < 30.0
    assert task_gui.time.text() == "just now"

    # Complete the task
    wait_animations_finished(qtbot, root_gui)
    assert not task_mod.done
    click_and_wait(qtbot, task_gui.button)
    assert task_mod.done
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 1
    assert len(root_mod.queues) == len(root_gui.findChildren(Queue)) == 1

    # Collapse the queue
    orig_height = queue_gui.tasks_frame.height()
    click_and_wait(qtbot, queue_gui.header.collapse_queue)
    wait_animations_finished(qtbot, root_gui)
    assert queue_gui.tasks_frame.height() == 0

    # Uncollapse the queue
    click_and_wait(qtbot, queue_gui.header.collapse_queue)
    wait_animations_finished(qtbot, root_gui)
    assert queue_gui.tasks_frame.height() == orig_height

    # Uncomplete the task
    wait_animations_finished(qtbot, root_gui)
    click_and_wait(qtbot, task_gui.button)
    assert not task_mod.done
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 1
    assert len(root_mod.queues) == len(root_gui.findChildren(Queue)) == 1

    # Complete it through the model
    wait_animations_finished(qtbot, root_gui)
    task_mod.done = True
    assert task_mod.done
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 1
    assert len(root_mod.queues) == len(root_gui.findChildren(Queue)) == 1

    # Delete the task
    destroyed = qtbot.wait_signal(task_gui.destroyed)
    click_and_wait(qtbot, task_gui.delete_button)
    wait_animations_finished(qtbot, root_gui)
    destroyed.wait()
    assert len(queue_mod.tasks) == len(queue_gui.findChildren(Task)) == 0
    assert len(root_mod.queues) == len(root_gui.findChildren(Queue)) == 1

    # Delete the queue
    destroyed = qtbot.wait_signal(queue_gui.destroyed)
    # ToDo: use click_and_wait(qtbot, queue_gui.delete_queue) instead of .remove
    root_mod.queues.remove(queue_mod)
    wait_animations_finished(qtbot, root_gui)
    destroyed.wait()
    assert len(root_mod.queues) == len(root_gui.findChildren(Queue)) == 0
