from contextlib import contextmanager

import pytest

from aside import config, resources


@contextmanager
def replaced(obj, attr, value):
    """Temporarily replace `obj.attr` with `value` (and then restore the original)."""
    old = getattr(obj, attr)
    setattr(obj, attr, value)
    try:
        yield value
    finally:
        setattr(obj, attr, old)


@pytest.fixture
def tmp_resource_root(tmp_path):
    """Context manager, that temporarily replaces the resource root with a tmp path."""
    return replaced(resources, "root", tmp_path)


def test_plain_resource(tmp_resource_root):
    """Test that a plain resource can be loaded with `get_resource`."""
    name = "myresource"
    contents = "{{contents}} {with curlies} }}unbalanced{"
    with tmp_resource_root as root:
        (root / name).write_text(contents)
        assert resources.get_resource(name) == contents


def test_missing_resource(tmp_resource_root):
    """Test that a plain resource can be loaded with `get_resource`."""
    with tmp_resource_root:
        with pytest.raises(RuntimeError, match="^Missing resource 'myresource'$"):
            resources.get_resource("myresource")


def test_interpolated_resource(monkeypatch, tmp_resource_root):
    """Test that a interpolated resource can be loaded with `get_resource`."""
    name = "myresource"
    int_name = "myresource.int"
    verbose = str(config.config.verbose)

    contents = "{kept as is} {{config.verbose}} } } unbalanced, also kept {"
    int_contents = "{kept as is} " + verbose + " } } unbalanced, also kept {"
    keyerror_int_contents = "{{doesntexist}}"
    valueerror_int_contents = "{{invalidformat"

    with tmp_resource_root as root:
        (root / name).write_text(contents)
        assert resources.get_resource(name) == contents

        (root / int_name).write_text(contents)
        assert resources.get_resource(name, interpolated=False) == contents
        assert resources.get_resource(name) == int_contents

        (root / int_name).write_text(keyerror_int_contents)
        with pytest.raises(KeyError):
            resources.get_resource(name)

        (root / int_name).write_text(valueerror_int_contents)
        with pytest.raises(ValueError):
            resources.get_resource(name)


def test_resources_import():
    """`aside.resources` must export a `root` variable."""
    assert hasattr(resources, "root")


def test_get_svg():
    """`get_svg` must be able to load a valid svg icon."""
    icon = resources.get_svg("icon")
    assert b"</svg>" in icon
