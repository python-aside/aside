import pytest

from aside import version


def test_version_import():
    """`aside.version` must export a `version` variable."""
    assert hasattr(version, "version")


def test_reject_wrong_version():
    """`aside.version` must fail if the git tag and version don't match."""
    fake_version = "not_a_valid_version"
    with pytest.raises(RuntimeError):
        version.get_version(commit_tag=fake_version)


def test_accept_current_version():
    """`aside.version` must succeed if the git tag and base_version do match."""
    correct_version = version.base_version
    assert correct_version == version.get_version(commit_tag=correct_version)


def test_use_short_sha():
    """`aside.version` must include the git sha in the version, if available."""
    short_sha = "some_short_sha_value"
    assert short_sha in version.get_version(short_sha=short_sha)


def test_source_install_or_pkg_resources():
    """Non-source installs of the package should resolve to the current version."""
    isolated_version = version.get_version()
    if not isolated_version.endswith(".post0+installed.from.source"):
        assert isolated_version == version.version
