from datetime import datetime

import pytest

from aside.models.database import Database
from aside.models.models import Queue, QueueManager, Task


def test_simple_serialization(tmp_path):
    """Test serialization and runtime changes to Tasks and Queues."""
    queue_manager = QueueManager()
    with Database(queue_manager, tmp_path):
        assert (tmp_path / "aside" / ".lock").exists()
        first_queue = Queue(name="Test Queue", tasks=[Task() for _ in range(2)])
        queue_manager.queues.add(first_queue)
        second_queue = Queue()
        queue_manager.queues.add(second_queue)
        special_task = Task()
        second_queue.name = "New name!"
        second_queue.tasks.add(special_task)
        special_task.text = "New text!"
        second_queue.tasks.discard(special_task)
        for queue in queue_manager.queues.values():
            for task in queue.tasks.values():
                assert (tmp_path / "aside" / queue.uuid / f"{task.uuid}.json").exists()
            assert (tmp_path / "aside" / f"{queue.uuid}.json").exists()
        queue_manager.queues.discard(first_queue)
        queue_manager.queues.discard(second_queue)
    assert not (tmp_path / "aside" / ".lock").exists()
    assert len(list((tmp_path / "aside").iterdir())) == 0


def test_manager_population(tmp_path):
    """Test serialization on observers and subsequent repopulation.

    Fill queue manager with data in runtime, then connect to same Database and check
    correct serialization.
    """
    queue_manager = QueueManager()
    with Database(queue_manager, tmp_path):
        naive_time = datetime.now()
        aware_time = naive_time.astimezone()
        first_queue = Queue(
            name="Test Queue",
            tasks=[Task(text="Task text!", deadline=aware_time) for _ in range(2)],
        )
        queue_manager.queues.add(first_queue)
    queue_manager.drop_observers()
    new_queue_manager = QueueManager()
    with Database(new_queue_manager, tmp_path):
        assert len(queue_manager.queues) == 1
        for queue in queue_manager.queues.values():
            assert len(queue.tasks) == 2
            assert queue.name == "Test Queue"
            for task in queue.tasks.values():
                assert task.text == "Task text!"
                assert task.deadline == aware_time


def test_db_locking(tmp_path):
    """Try to use Database without acquiring lock and with double acquiring."""
    queue_manager = QueueManager()
    Database(queue_manager, tmp_path)
    with pytest.raises(RuntimeError):
        queue_manager.queues.add(Queue())
    with Database(queue_manager, tmp_path):
        with pytest.raises(RuntimeError):
            new_queue_manager = QueueManager()
            with Database(new_queue_manager, tmp_path):
                pass
