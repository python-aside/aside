import pytest
from attr.exceptions import FrozenAttributeError

from aside.boilerplate.observable import Event, EventType, ObservableCollection
from aside.models.models import Queue, QueueManager, Task


def test_structure_creation():
    """Integration test for models and observable event monitoring."""
    trigger_observer = []

    def discard_observer(event: Event):
        assert event.event_type is EventType.DISCARD
        assert isinstance(event.get_nested_object(0), QueueManager)
        trigger_observer.append("Manager")

    def manager_observer(event: Event):
        assert event.event_type in [EventType.ADD, EventType.CHANGE]
        raiser = event.get_nested_object()
        if isinstance(raiser, Task):
            trigger_observer.append("Task")
        if isinstance(raiser, Queue):
            trigger_observer.append("Queue")
        if isinstance(raiser, QueueManager):
            trigger_observer.append("Manager")

    manager = QueueManager()
    manager.subscribe(manager_observer, event_types=[EventType.ADD, EventType.CHANGE])
    manager.subscribe(
        discard_observer, event_types=[EventType.DISCARD]
    )  # TODO: add scalar variant of argument?
    for _ in range(2):
        manager.queues.add(Queue(tasks=[Task() for _ in range(5)]))
    queue: Queue = list(manager.queues.values())[0]
    queue.tasks.add(Task())
    assert len(trigger_observer) == 3
    list(queue.tasks.values())[0].text = "New text!"
    manager.queues.discard(queue)
    assert trigger_observer == ["Queue", "Queue", "Task", "Manager"]


def test_frozen_collections():
    """Test that after creation you can't assign new collection."""
    queue_manager = QueueManager()
    with pytest.raises(FrozenAttributeError):
        queue_manager.queues = ObservableCollection()
    queue = Queue()
    with pytest.raises(FrozenAttributeError):
        queue.tasks = ObservableCollection()
