FROM python:3.6

RUN apt-get update &&                           \
    apt-get install -y                          \
        herbstluftwm                            \
        libdbus-1-3                             \
        libgl1                                  \
        libxcb-icccm4                           \
        libxcb-image0                           \
        libxcb-keysyms1                         \
        libxcb-randr0                           \
        libxcb-render-util0                     \
        libxcb-util0                            \
        libxcb-xfixes0                          \
        libxcb-xinerama0                        \
        libxcb-xinput0                          \
        libxkbcommon-x11-0                      \
        locales                                 \
        x11-xserver-utils                       \
        xvfb                                    \
        &&                                      \
    sed --in-place                              \
        '/en_GB.UTF-8/s/^# *//g'                \
        /etc/locale.gen                         \
        &&                                      \
    locale-gen &&                               \
    rm -rf /var/lib/apt/lists/*

RUN printf '%s\n'                               \
        '#!/bin/sh'                             \
        ''                                      \
        'echo "Starting Xvfb"'                  \
        'Xvfb ${DISPLAY} \'                     \
        '   -screen ${SCREEN} \'                \
        '   ${MODE} \'                          \
        '   -ac -noreset -nolisten tcp \'       \
        '   ${XVFB_EXTENSIONS} &'               \
        'XVFB_PID=$!'                           \
        'sleep 1'                               \
        ''                                      \
        'echo "Starting herbstluftwm"'          \
        'herbstluftwm &'                        \
        'HSWM_PID=$!'                           \
        'sleep 1'                               \
        ''                                      \
        'echo "Running main script"'            \
        '"${@}"'                                \
        'EXIT_CODE=$?'                          \
        ''                                      \
        'echo "Stopping herbstluftwm"'          \
        'sleep 1'                               \
        'kill ${HSWM_PID}'                      \
        'wait ${HSWM_PID}'                      \
        ''                                      \
        'echo "Stopping Xvfb"'                  \
        'sleep 1'                               \
        'kill ${XVFB_PID}'                      \
        'wait ${XVFB_PID}'                      \
        ''                                      \
        'exit ${EXIT_CODE}'                     \
        >/usr/bin/entrypoint &&                 \
    chmod +x /usr/bin/entrypoint

ENV DISPLAY=:99.0
ENV SCREEN=0
ENV MODE=1920x1080x24
ENV XVFB_EXTENSIONS="+extension GLX +render"
ENV LANG="en_GB.UTF-8"
ENV LANGUAGE="en_GB:en"
ENV LC_ALL="en_GB.UTF-8"
ENTRYPOINT ["/usr/bin/entrypoint"]
