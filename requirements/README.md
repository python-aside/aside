# Requirements

The files in this directory describe the development and CI/CD environment
requirements. The requirements of the `aside` package itself are described in
the `install_requires` section of the `setup.cfg` file.

## `.in` files

The top-level package requirements are specified in separate `.in` files
depending on their category: `docs`, `lint`, `meta`, `push` or `test`.
The `aside` package itself is not included as a direct dependency, but all the
runtime dependencies of `aside` are included by sourcing the `setup.cfg` file.

## `_txt` file

The `_txt` file is the equivalent of a `requirements.txt` file. It is used to
pin the exact versions used in CI/CD to ensure reproducible errors and identical
development environments. Do not edit it manually.

## Requirements compilation

The contents of the `_txt` file are automatically generated from the `.in`
files. You can compile the `_txt` file by running the
`scripts/venv_requirements_compile.sh` script. The lines inside each `.in` file
**must** be sorted to ensure deterministic `_txt` file generation.

The `venv_check` job in CI/CD verifies, that the `.in` files are sorted and that
the `_txt` file is up-to-date with the `.in` files. If you changed the
`.in` files or the `install_requires` section in the `setup.cfg` file and didn't
run `scripts/venv_requirements_compile.sh`, your commits will not pass the CI/CD
pipeline.
