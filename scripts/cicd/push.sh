#!/bin/sh
. "$(dirname "${0}")/../_common.sh"
activate_venv

# region Build the package sdist and wheel
section_start build "Building the package sdist and wheel"

mkdir -p .out/build

# We use "--no-isolation", because we use our own venv isolation mechanism
run_quiet "build the package sdist and wheel" \
    "${PYTHON}" -m build \
    --no-isolation \
    --sdist \
    --wheel \
    --outdir .out/build
rm -rf build

section_end build
# endregion

# region Check the package
section_start check "Checking the built package"
run_quiet "check the built sdist and wheel with twine" \
    "${PYTHON}" -m twine check \
    --strict \
    .out/build/*
section_end check
# endregion

# region Upload the package
if [ -z ${TWINE_REPOSITORY_URL+x} ] ||
    [ -z ${TWINE_USERNAME+x} ] ||
    [ -z ${TWINE_PASSWORD+x} ]; then
    if [ -z "${SKIP_UPLOAD+x}" ]; then
        echo "TWINE_REPOSITORY_URL, TWINE_USERNAME and TWINE_PASSWORD"
        echo "environment variables are not set, skip uploading the package."

        exit 1
    fi
else
    section_start upload "Uploading the package to ${TWINE_REPOSITORY_URL}"
    ${PYTHON} -m twine upload \
        --verbose \
        --non-interactive \
        --skip-existing \
        .out/build/*
    section_end upload
fi
# endregion
