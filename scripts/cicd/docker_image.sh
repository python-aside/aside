#!/bin/sh
. "$(dirname "${0}")/../_common.sh"

if [ -z ${CI+x} ]; then
    echo "This script is only intended to be run under CICD."
    exit 1
fi

# region Log in to the GitLab registry
section_start login "Logging in to the GitLab registry"
docker \
    login \
    -u "${CI_REGISTRY_USER}" \
    -p "${CI_REGISTRY_PASSWORD}" \
    "${CI_REGISTRY}"
section_end login
# endregion

# region Check if the image already exists
section_start check "Checking if the image already exists"

MD5SUM=$(md5sum requirements/Dockerfile | cut -d' ' -f1)

MD5SUM_IMAGE="${CI_REGISTRY_IMAGE}:md5sum-${MD5SUM}"
PIPELINE_IMAGE="${CI_REGISTRY_IMAGE}:pipeline-${CI_PIPELINE_ID}"
LATEST_IMAGE="${CI_REGISTRY_IMAGE}:latest"

echo "Attempting to reuse a previously built image based on md5sum."
echo "Dockerfile md5sum: ${MD5SUM}"
docker manifest create "${PIPELINE_IMAGE}" "${MD5SUM_IMAGE}" ||
    NO_IMAGE=1

section_end check
# endregion

# region Build the docker image (if unavailable)
if [ -n "${NO_IMAGE+x}" ]; then
    echo "No image found, starting docker build"

    section_start pull_latest "Pulling the latest image (for build cache)"
    docker pull "${LATEST_IMAGE}" || true
    section_end pull_latest

    section_start build "Building the image"
    docker \
        build \
        --cache-from "${LATEST_IMAGE}" \
        --tag "${MD5SUM_IMAGE}" \
        - <requirements/Dockerfile
    section_end build

    section_start push "Pushing the built image"
    docker push "${MD5SUM_IMAGE}"
    section_end push

    section_start push_latest "Updating the latest image manifest"
    docker manifest create "${LATEST_IMAGE}" "${MD5SUM_IMAGE}"
    docker manifest push "${LATEST_IMAGE}"
    section_end push_latest

    section_start push_pipeline "Updating the pipeline image manifest"
    docker manifest create "${PIPELINE_IMAGE}" "${MD5SUM_IMAGE}"
    docker manifest push "${PIPELINE_IMAGE}"
    section_end push_pipeline
else
    echo "Found existing image for this md5sum, skipping image build"

    section_start push_pipeline "Updating the pipeline image manifest"
    docker manifest push "${PIPELINE_IMAGE}"
    section_end push_pipeline
fi
# endregion
