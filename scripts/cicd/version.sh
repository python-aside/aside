#!/bin/sh
# This script is intended to be run on the registry.gitlab.com/gitlab-org/release-cli docker image
# release-cli is based on busybox, which has completely broken sed/grep utilities,
# so the script is 100 times more complicated, then it needs to be </rant>
cd "$(dirname "${0}")/../.."
set -eu

if [ -z ${VERSION_FILE+x} ]; then
    echo "VERSION_FILE environment variable is not set, aborting." >&2
    exit 1
fi

# region Extract the package version
echo "Extracting package version" >&2

PATTERN="^__version__ = \"\([0-9.]*\)\"$"
MATCHES=$(grep -c "${PATTERN}" "${VERSION_FILE}")
if [ "${MATCHES}" != "1" ] || ! VERSION=$(
    grep "${PATTERN}" "${VERSION_FILE}" |
        sed "s/${PATTERN}/\1/g"
) || [ -z "${VERSION}" ]; then
    echo "Couldn't extract the version number from ${VERSION_FILE}" >&2
    exit 1
fi

echo "Successfully extracted version number ${VERSION} from ${VERSION_FILE}" >&2
echo "${VERSION}"
# endregion
