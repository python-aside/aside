#!/bin/sh
. "$(dirname "${0}")/../_common.sh"
activate_venv

# region Test the package
section_start test "Testing the package"

mkdir -p .out

PYTHONPATH=src ${PYTHON} -m pytest \
    >.out/pytest.txt 2>&1 ||
    PYTEST_FAIL=1

${PYTHON} -m coverage xml \
    >.out/coverage.txt 2>&1 ||
    COVERAGE_FAIL=1

${PYTHON} -m coverage report \
    >>.out/coverage.txt 2>&1 ||
    COVERAGE_FAIL=1

section_end test
# endregion

# region Generate badges
section_start badges "Generating badges"

sed 's/^TOTAL *\([0-9][0-9]*  *\)*\([0-9.]*\)% */\2/p' \
    --quiet \
    .out/coverage.txt \
    >.out/coverage.score
file_default .out/coverage.score 0

${PYTHON} -c "exit($(cat .out/coverage.score) < 100.0)" ||
    COVERAGE_SHOW=1

if [ -n "${PYTEST_FAIL+x}" ]; then
    echo "failing" >.out/pytest.score
else
    echo "passing" >.out/pytest.score
fi

gen_badge pytest passing
gen_badge coverage 100

section_end badges
# endregion

# region Report linter results
if [ -n "${PYTEST_FAIL+x}" ] || [ -n "${CI+x}" ] || [ -n "${VERBOSE+x}" ]; then
    section_start report_pytest "Reporting pytest results"
    cat .out/pytest.txt
    section_end report_pytest
fi

if [ -n "${COVERAGE_SHOW+x}" ] || [ -n "${CI+x}" ] || [ -n "${VERBOSE+x}" ]; then
    section_start report_coverage "Reporting coverage results"
    cat .out/coverage.txt
    section_end report_coverage
fi

if [ -n "${PYTEST_FAIL+x}" ] ||
    [ -n "${COVERAGE_FAIL+x}" ]; then
    exit 70
fi
:
# endregion
