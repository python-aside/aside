#!/bin/sh
# This script is intended to be run on the registry.gitlab.com/gitlab-org/release-cli docker image
# release-cli is based on busybox, which has completely broken sed/grep utilities,
# so the script is 100 times more complicated, then it needs to be </rant>
cd "$(dirname "${0}")/../.."
set -eu

if [ -z ${AUTO_TAG_TOKEN+x} ] || [ -z "${CI_COMMIT_SHA+x}" ]; then
    echo "This script is only intended to be run under CICD." >&2
    echo "AUTO_TAG_TOKEN environment variable is not set, aborting." >&2
    exit 1
fi

VERSION=$(VERSION_FILE=src/aside/_version.py scripts/cicd/version.sh)

# region Create release tag
echo "Creating release tag" >&2

# We use a Maintainers private token here to create a protected tag
release-cli \
    --private-token "${AUTO_TAG_TOKEN}" \
    create \
    --tag-name "${VERSION}" \
    --description "Created automatically with auto_tag and release-cli." \
    --ref "${CI_COMMIT_SHA}"

echo "Done" >&2
# endregion
