#!/bin/sh
. "$(dirname "${0}")/../_common.sh"
activate_venv

# region Generate the documentation
section_start docs "Generating the documentation"

mkdir -p .out

if [ -e docs/devdoc ]; then
    echo "The docs/devdoc directory should not be present before documentation build."
    echo
    if [ -n "${CI+x}" ]; then
        echo "Did you accidentally commit it?"
        exit 1
    else
        echo "It is automatically generated before each documentation build and"
        echo "it should be automatically removed after each documentation build."
        echo
        echo "If you keep seeing this message, something is wrong."
        rm -rf docs/devdoc
    fi
fi

PYTHONPATH=src ${PYTHON} -m sphinx \
    -b dirhtml \
    -d .cache/doctrees/dirhtml \
    -j auto \
    --color \
    -Wnv --keep-going \
    docs .out/sphinx.dirhtml \
    >.out/sphinx.txt 2>&1 ||
    SPHINX_FAIL=1

rm -rf docs/devdoc

section_end docs
# endregion

# region Report documentation generation results
if [ -n "${SPHINX_FAIL+x}" ] || [ -n "${CI+x}" ] || [ -n "${VERBOSE+x}" ]; then
    section_start report_sphinx "Reporting sphinx results"
    cat .out/sphinx.txt
    section_end report_sphinx
fi

if [ -n "${SPHINX_FAIL+x}" ]; then
    exit 70
fi
:
# endregion
