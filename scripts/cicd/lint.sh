#!/bin/sh
. "$(dirname "${0}")/../_common.sh"
activate_venv

# region Run linter tools
section_start lint "Running linter tools"

mkdir -p .out

${PYTHON} -m isort . \
    --show-files \
    >.out/isort.txt 2>&1 ||
    ISORT_FAIL=1

${PYTHON} -m isort . \
    --diff \
    --check \
    >>.out/isort.txt 2>&1 ||
    ISORT_FAIL=1

${PYTHON} -m black . \
    --check \
    --diff \
    >.out/black.txt 2>&1 ||
    BLACK_FAIL=1

${PYTHON} -m pylint tests src/* \
    --verbose \
    >.out/pylint.txt 2>&1 ||
    PYLINT_FAIL=1

${PYTHON} -m pydocstyle tests src \
    --count \
    --explain \
    --source \
    --verbose \
    >.out/pydocstyle.txt 2>&1 ||
    PYDOCSTYLE_SHOW=1

section_end lint
# endregion

# region Generate badges
section_start badges "Generating badges"

sed 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' \
    --quiet \
    .out/pylint.txt \
    >.out/pylint.score
file_default .out/pylint.score 0

${PYTHON} -c "exit($(cat .out/pylint.score) < 10.0)" ||
    PYLINT_SHOW=1

sed 's/^\([0-9][0-9]*\)$/\1/p' \
    --quiet \
    .out/pydocstyle.txt \
    >.out/pydocstyle.errors
# pydocstyle doesn't output a count with --count, when there were no errors
file_default .out/pydocstyle.errors 40 # = 0.0 score

${PYTHON} -c "print(10.0 - $(cat .out/pydocstyle.errors)/4.0)" \
    >.out/pydocstyle.score

${PYTHON} -c "exit($(cat .out/pydocstyle.score) < 5.0)" ||
    PYDOCSTYLE_FAIL=1

if [ -n "${ISORT_FAIL+x}" ]; then
    echo "failing" >.out/isort.score
else
    echo "passing" >.out/isort.score
fi

if [ -n "${BLACK_FAIL+x}" ]; then
    echo "failing" >.out/black.score
else
    echo "passing" >.out/black.score
fi

gen_badge isort passing
gen_badge black passing
gen_badge pylint 10
gen_badge pydocstyle 10

section_end badges
# endregion

# region Report linter results
if [ -n "${ISORT_FAIL+x}" ] || [ -n "${CI+x}" ] || [ -n "${VERBOSE+x}" ]; then
    section_start report_isort "Reporting isort results"
    cat .out/isort.txt
    section_end report_isort
fi

if [ -n "${BLACK_FAIL+x}" ] || [ -n "${CI+x}" ] || [ -n "${VERBOSE+x}" ]; then
    section_start report_black "Reporting black results"
    cat .out/black.txt
    section_end report_black
fi

if [ -n "${PYLINT_SHOW+x}" ] || [ -n "${CI+x}" ] || [ -n "${VERBOSE+x}" ]; then
    section_start report_pylint "Reporting pylint results"
    cat .out/pylint.txt
    section_end report_pylint
fi

if [ -n "${PYDOCSTYLE_SHOW+x}" ] || [ -n "${CI+x}" ] || [ -n "${VERBOSE+x}" ]; then
    section_start report_pydocstyle "Reporting pydocstyle results"
    cat .out/pydocstyle.txt
    section_end report_pydocstyle
fi

if [ -n "${ISORT_FAIL+x}" ] ||
    [ -n "${BLACK_FAIL+x}" ] ||
    [ -n "${PYLINT_FAIL+x}" ] ||
    [ -n "${PYDOCSTYLE_FAIL+x}" ]; then
    exit 70
fi
:
# endregion
