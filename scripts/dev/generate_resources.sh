#!/bin/sh
. "$(dirname "${0}")/../_common.sh"

if [ -n "${VIRTUAL_ENV+x}" ]; then
    echo "This script should not be run inside a virtual environment."
    echo ""
    echo "Please deactivate and try again."
    exit 1
fi

FONTAWESOME_URL="https://use.fontawesome.com/releases/v5.15.3/fontawesome-free-5.15.3-desktop.zip"
MATERIAL_URL="https://github.com/google/material-design-icons/archive/refs/tags/4.0.0.zip"
VARELA_URL="https://fonts.google.com/download?family=Varela%20Round"
LOGO_TEXT="'ASIDE'"
PREFIX="resources/"

# region Dependencies
mkdir -p .out

if [ ! -f .out/accepted_generate_logo_deps ]; then
    echo "This script will use your system python (not the one installed in venv)"
    echo "Additionally, it will need the following programs/packages:"
    echo ""
    echo "  from pip:"
    echo "    - click"
    echo "    - svgpathtools"
    echo "    - svgwriter"
    echo ""
    echo "  from your package manager:"
    echo "    - FontForge"
    echo "    - ImageMagick"
    echo "    - SVGO"
    echo "    - wget (or manually download ${VARELA_URL} to .out/VarelaRound.zip)"
    echo "    - unzip (or manually extract .out/VarelaRound.zip to .out/VarelaRound-Regular.ttf)"
    echo ""
    echo "Please manually install them before proceeding."
    echo ""

    printf "Proceed running this script? [Y/n] "
    read -r response
    case "$response" in
    [yY][eE][sS] | [yY] | '') ;;
    *) exit 0 ;;
    esac

    # Only ask once
    touch .out/accepted_generate_logo_deps
fi
# endregion

# region Varela Round Font
if [ ! -f .out/VarelaRound-Regular.ttf ]; then
    if [ ! -f .out/VarelaRound.zip ]; then
        section_start download_varela_round "Downloading Varela Round Font"
        wget "${VARELA_URL}" \
            -O .out/VarelaRound.zip
        section_end download_varela_round
    fi
    section_start extract_varela_round "Extracting Varela Round Font"
    rm -rf .out/VarelaRound-Regular.ttf
    unzip .out/VarelaRound.zip -d .out VarelaRound-Regular.ttf
    section_end extract_varela_round
fi

section_start extract_glyphs "Extracting Letter Glyphs"
mkdir -p .out/VarelaRound
# This actually uses the system python since installing fontforge into a venv is currently not possible
fontforge -c "
import fontforge
for glyph in fontforge.open('.out/VarelaRound-Regular.ttf').selection.select(*${LOGO_TEXT}).byGlyphs:
    glyph.export(f'.out/VarelaRound/{glyph.glyphname}.svg')
"
section_end extract_glyphs
# endregion

# region FontAwesome
if [ ! -d .out/FontAwesome ]; then
    if [ ! -f .out/FontAwesome.zip ]; then
        section_start download_fontaweseome "Downloading FontAwesome Font"
        wget "${FONTAWESOME_URL}" \
            -O .out/FontAwesome.zip
        section_end download_fontawesome
    fi
    section_start extract_fontawesome "Extracting FontAwesome SVGs"
    rm -rf .out/fontawesome-free-5.15.3-desktop
    unzip .out/FontAwesome.zip -d .out 'fontawesome-free-5.15.3-desktop/svgs/*'
    mv .out/fontawesome-free-5.15.3-desktop/svgs .out/FontAwesome
    rm -rf .out/fontawesome-free-5.15.3-desktop
    section_end extract_fontawesome
fi
# endregion

# region Material
if [ ! -d .out/Material ]; then
    if [ ! -f .out/Material.zip ]; then
        section_start download_fontaweseome "Downloading Material Font"
        wget "${MATERIAL_URL}" \
            -O .out/Material.zip
        section_end download_material
    fi
    section_start extract_material "Extracting Material SVGs"
    rm -rf .out/material-design-icons-4.0.0
    unzip .out/Material.zip -d .out 'material-design-icons-4.0.0/src/*'
    mv .out/material-design-icons-4.0.0/src .out/Material
    rm -rf .out/material-design-icons-4.0.0
    section_end extract_material
fi
# endregion

mkdir -p "${PREFIX}"
for f in icon logo; do
    section_start make_${f} "Generating ${f}"
    INTERP=0 python scripts/svg make_${f} "${PREFIX}"${f}.svg
    INTERP=1 python scripts/svg make_${f} "${PREFIX}"${f}.svg.int
    section_end make_${f}

    section_start optimize_${f} "Optimizing ${f}"
    svgo --input "${PREFIX}"${f}.svg --multipass --output "${PREFIX}"${f}.svg
    svgo --input "${PREFIX}"${f}.svg.int --multipass --output "${PREFIX}"${f}.svg.int
    section_end optimize_${f}
done

for f in add delete info queue queue_add queue_closed settings task task_done task_warn; do
    section_start make_${f} "Generating ${f}"
    INTERP=1 python scripts/svg make_icon --content ${f} "${PREFIX}"${f}.svg.int
    section_end make_${f}

    section_start optimize_${f} "Optimizing ${f}"
    svgo --input "${PREFIX}"${f}.svg.int --multipass --output "${PREFIX}"${f}.svg.int
    section_end optimize_${f}
done

section_start convert_icon_favicon "Converting icon to favicon"
convert -background none "${PREFIX}"icon.svg -trim -define icon:auto-resize=64,48,32,16 -strip "${PREFIX}"favicon.ico
section_end convert_icon_favicon
