#!/bin/sh
. "$(dirname "${0}")/../_common.sh"
activate_venv

if [ -n "${VERBOSE+x}" ]; then
    INCLUDE_MERGED_YAML=True
else
    INCLUDE_MERGED_YAML=False
fi

# region Validate .gitlab-ci.yml
section_start validate_yaml "Validating .gitlab-ci.yml"

${PYTHON} -c "
import sys
import requests

resp = requests.post(
    'https://gitlab.com/api/v4/ci/lint',
    data={
        'content': open('.gitlab-ci.yml').read(),
        'include_merged_yaml': ${INCLUDE_MERGED_YAML},
    },
).json()

if ${INCLUDE_MERGED_YAML}:
    print(resp['merged_yaml'])

print(resp['status'])
"

section_end validate_yaml
# endregion
