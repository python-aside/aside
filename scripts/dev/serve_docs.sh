#!/bin/sh
. "$(dirname "${0}")/../_common.sh"

echo
echo "This script will build the documentation and then start a simple python HTTP server."
echo "While the HTTP server is running, you can press:"
echo
echo "    r -- to rebuild the documentation and restart the server"
echo "    q -- to stop this script"
echo

activate_venv

build() {
    scripts/cicd/docs.sh || true
}

serve() {
    section_start serve "Running python http server"
    while ncat -z localhost 8000; do
        sleep 0.1
    done
    cd .out/sphinx.dirhtml
    ${PYTHON} -m http.server &
    SERVER_PID=$!
    cd ../..
    section_end serve
}

trap 'kill $(jobs -p)' EXIT
build
serve
while :; do
    # shellcheck disable=SC2039
    read -r -n1 ACTION
    printf '\r \r'
    if [ "${ACTION}" = "q" ]; then
        echo
        break
    elif [ "${ACTION}" = "r" ]; then
        build
        kill ${SERVER_PID}
        serve
        # shellcheck disable=SC2039
        read -r -n 1024 -t 0.1 || true
    fi
done
