#!/bin/sh
. "$(dirname "${0}")/../_common.sh"
activate_venv

scripts/cicd/lint.sh || FAIL=1
scripts/cicd/test.sh || FAIL=1

[ -n "${FAIL+x}" ] && exit 1
:
