#!/bin/sh
. "$(dirname "${0}")/../_common.sh"
activate_venv

section_start isort "Running isort"
${PYTHON} -m isort .
section_end isort

section_start black "Running black"
${PYTHON} -m black .
section_end black
