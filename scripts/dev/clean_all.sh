#!/bin/sh
. "$(dirname "${0}")/../_common.sh"

remove_matching_dirs() {
    echo "Removing ${*}"
    find . -path '*/'"${1}"'/*' -delete
    find . -type d -name "${1}" -empty -delete
}

remove_matching_files_from() {
    echo "Removing ${*}"
    find "${1}" -type f -path '*/'"${2}" -delete
}

remove_in_root() {
    echo "Removing ${*}"
    rm -rf "${@}"
}

section_start clean "Cleaning project directory"

# Keep this list in sync with the contents of .gitignore
remove_matching_dirs '*.egg-info'
remove_in_root ".cache"
remove_in_root ".out"
remove_in_root ".venv"
remove_matching_dirs '__pycache__'
remove_in_root "build"
remove_in_root "docs/devdoc"

section_end clean
