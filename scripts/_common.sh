#!/bin/sh
cd "$(dirname "${0}")/../.."

# region Variables
_STATE_DEF="$(set +o)"
set -eu
_STATE_COM="$(set +o)"

PYTHON_VERSION="3.6"
if command -v "python${PYTHON_VERSION}" 1>/dev/null 2>&1; then
    SYSTEM_PYTHON="python${PYTHON_VERSION}"
else
    echo "You don't seem to have python${PYTHON_VERSION} installed."
    echo "Proceeding with default python3, but things will likely break."
    SYSTEM_PYTHON="python3"
fi

if [ -z "${COLUMNS+x}" ]; then
    if [ -n "${CI+x}" ]; then
        COLUMNS="157"
    else
        COLUMNS=$(tput cols -T "dumb")
    fi
    export COLUMNS
fi
# endregion

# region Functions
activate_venv() {
    if [ -z "${VIRTUAL_ENV+x}" ] || [ -n "${FORCE_SWITCH_VENV+x}" ]; then
        section_start activate "Activating virtual environment"

        if [ ! -f ".venv/bin/activate" ]; then
            echo "The virtual environment is required for this script"
            echo
            echo "Did you forget to run \`venv/create.sh\`?"
            exit 1
        fi

        # This weird wrapper is required so that `set -eu`
        # doesn't break inside the "activate" venv script
        eval "${_STATE_DEF}"
        # shellcheck source=/dev/null
        . ".venv/bin/activate" 2>&1
        eval "${_STATE_COM}"

        section_end activate
    fi
    export PYTHON="${VIRTUAL_ENV}/bin/python"
}

file_default() {
    name=${1}
    default=${2}
    if ! grep -q '[^[:space:]]' "${name}"; then
        printf "%s" "${default}" >"${name}"
    fi
}

gen_badge() {
    ${PYTHON} -c "
import anybadge

label = '${1}'
style = '${2}'
value = '$(cat ".out/${1}.score")'

if style == '10':
    value = int(10 * float(value))
    style = 'percent'

elif style == '100':
    value = int(float(value))
    style = 'percent'

thresholds = {
    'passing': {
        'failing': 'red',
        'passing': 'green',
    },
    'percent': {
        50: 'red',
        60: 'orange',
        70: 'yellow',
        80: 'yellowgreen',
        90: 'olive',
        100: 'green',
    },
}[style]

if style == 'percent':
    value_suffix = '%'
else:
    value_suffix = ''

b = anybadge.Badge(
    label,
    value,
    thresholds=thresholds,
    value_suffix=value_suffix,
)
b.num_label_padding_chars = (4*19 - b.label_width) / (2 * b.font_width)
b.num_value_padding_chars = (4*15 - b.value_width) / (2 * b.font_width)
b.write_badge('.out/${1}.svg', overwrite=True)
"
}

run_quiet() {
    action="${1}"
    shift
    if ! STATUS=$("${@}" 2>&1); then
        echo "Something went wrong, while trying to ${action}"
        echo
        echo "${STATUS}"
        exit 1
    elif [ -n "${CI+x}" ] || [ -n "${VERBOSE+x}" ]; then
        echo "${STATUS}"
    fi
}

section_start() {
    section="${1}"
    shift
    name="${*}"
    if [ -n "${CI+x}" ]; then
        printf "\nsection_start:%s:%s\r\e[0K\e[34;40m==> %s <==\e[K\e[0m\n\n" \
            "$(date +%s)" "${section}" "${name}"
    else
        printf "\e[0K\e[34;40m==> %s <==\e[K\e[0m\n" \
            "${name}"
    fi
}

section_end() {
    section="${1}"
    if [ -n "${CI+x}" ]; then
        printf "\nsection_end:%s:%s\r\e[0K\n" \
            "$(date +%s)" "${section}"
    fi
}
# endregion
