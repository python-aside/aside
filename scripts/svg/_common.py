import os
import pathlib
from typing import Callable, Generator, Optional, Tuple, Union

import click
import svgwrite.base  # noqa
import svgwrite.mixins  # noqa

Element = Union[svgwrite.base.BaseElement, svgwrite.mixins.Transform]


@click.group()
def commands() -> None:
    pass


def opacity(named: str) -> str:
    return "opacity:{{theme.icon_" + named + "_alpha}}"


def fill(named: str) -> str:
    return "fill:{{theme.icon_" + named + "_color}}"


def inplace_interpolate(path: str) -> None:
    from aside.resources import load_resource

    path = pathlib.Path(path)
    path.write_text(load_resource(path, do_interpolate=True))


def add_elements(
    d: Element,
    *objects: Element,
) -> Element:
    for o in objects:
        d.add(o)
    return d


def extend_style(elem: Element, style: Optional[str]) -> None:
    if style is None:
        return

    try:
        current = elem["style"]
    except KeyError:
        current = ""

    if current and not current.endswith(";"):
        current += ";"
    current += style
    elem["style"] = current


def make_hi_body_lo(
    d: svgwrite.Drawing,
    base: Element,
    offset: Tuple[int, int],
    style: Optional[str] = None,
    hi: bool = True,
    lo: bool = True,
    hi_style: Optional[str] = None,
    lo_style: Optional[str] = None,
) -> Generator[Element, None, None]:
    if lo:
        lo = base.copy()
        extend_style(lo, lo_style)
        lo.translate(*offset)
        yield lo

    if hi:
        hi = base.copy()
        extend_style(hi, hi_style)
        hi.translate(*(-o for o in offset))
        yield hi

    main = base.copy()
    extend_style(main, style)
    yield main


logo_text = "ASIDE"
logo_glyph_file = ".out/VarelaRound/{}.svg"
fa_glyph_file = ".out/FontAwesome/{}.svg"
material_glyph_file = ".out/Material/{}/materialicons/24px.svg"
material_round_glyph_file = ".out/Material/{}/materialiconsround/24px.svg"

base_size = 1024
shadow_size = base_size / 64

circle_padding = base_size / 16
circle_radius = (base_size - circle_padding) / 2
circle_center = (base_size / 2, base_size / 2)

text_size = 0.6 * base_size
text_start = (circle_center[0] + circle_radius, circle_center[1])

circle_style = fill("circle")
circle_hi_style = fill("circle_hi")
text_style = fill("text")
content_style = fill("content")
accent_style = fill("accent_content")
negative_style = fill("negative_content")
positive_style = fill("positive_content")
shadow_style = opacity("shadow")
