from typing import List, Tuple
from xml.etree import ElementTree

from _common import Element, add_elements, svgwrite
from svgpathtools import parse_path


def import_glyph(glyph_file: str) -> Tuple[float, float, float, float, List[str]]:
    svg = ElementTree.parse(glyph_file).getroot()
    mx, my, w, h = (int(v) for v in svg.attrib["viewBox"].split())
    yield mx, my, w, h
    for path in svg:
        if path.attrib.get("fill", None) == "none":
            continue
        path = parse_path(path.attrib["d"]).d().split()
        yield path


def glyph_to_path(
    d: svgwrite.Drawing,
    glyph_file: str,
    scale: float,
) -> Tuple[Element, float]:
    glyph = import_glyph(glyph_file)
    mx, my, w, h = next(glyph)
    paths = []
    for path in glyph:
        p = d.path()
        p.push(*path)
        p.scale(scale / h)
        p.translate(-(mx + w) / 2, -(my + h) / 2)
        paths.append(p)
    return add_elements(d.g(), *paths), w * (scale / h)
