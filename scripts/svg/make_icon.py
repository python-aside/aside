from _common import *
from _glyphs import glyph_to_path


def _make_content(
    fa_glyph_name: str,
    scale: float = 0.75,
    rotate: float = 0,
    glyph_file_fmt: str = material_round_glyph_file,
    style: str = content_style,
) -> Callable[[svgwrite.Drawing], Element]:
    def _maker(
        d: svgwrite.Drawing,
    ) -> Element:
        glyph_path = glyph_file_fmt.format(fa_glyph_name)
        glyph, _ = glyph_to_path(d, glyph_path, base_size)
        glyph.translate(*circle_center)
        glyph.rotate(rotate)
        glyph.scale(scale)
        return glyph, style

    return _maker


make_icon_content = _make_content(
    logo_text[0],
    scale=1.0,
    rotate=-90,
    glyph_file_fmt=logo_glyph_file,
    style=text_style,
)
make_add_content = _make_content(
    "content/add",
    style=accent_style,
)
make_delete_content = _make_content(
    "action/delete_outline",
    style=negative_style,
)
make_info_content = _make_content(
    "action/info_outline",
)
make_queue_content = _make_content(
    "navigation/expand_more",
    style=accent_style,
)
make_queue_closed_content = _make_content(
    "navigation/expand_more",
    style=accent_style,
    rotate=-90,
)
make_settings_content = _make_content(
    "action/settings",
)
make_task_content = _make_content(
    "toggle/radio_button_unchecked",
)
make_task_done_content = _make_content(
    "action/check_circle_outline",
    style=positive_style,
)
make_task_warn_content = _make_content(
    "alert/error_outline",
    style=negative_style,
)

make_queue_add_content = _make_content(
    "av/playlist_add",
    style=accent_style,
)


def make_icon(
    d: svgwrite.Drawing,
    content: str = "icon",
) -> Generator[Element, None, None]:
    yield from make_hi_body_lo(
        d,
        d.circle(r=circle_radius, center=circle_center),
        offset=(shadow_size, shadow_size),
        style=circle_style,
        hi_style=circle_hi_style,
        lo_style=shadow_style,
    )

    make_content = globals().get(f"make_{content}_content")
    if make_content is None:
        raise NotImplementedError(f"Icon content type '{content}' is not implemented.")

    content, content_style = make_content(d)
    yield from make_hi_body_lo(
        d,
        add_elements(d.g(), content),
        offset=(shadow_size, shadow_size),
        style=content_style,
        lo_style=shadow_style,
        hi=False,
    )


@commands.command("make_icon")
@click.argument("filename")
@click.option("--pretty", is_flag=True)
@click.option("--content", default="icon")
def main(filename: str, pretty: bool, content: str) -> Optional[int]:
    d = svgwrite.Drawing(size=(base_size, base_size))
    add_elements(d, *make_icon(d, content))
    d.saveas(filename, pretty=pretty)
    if not int(os.environ.get("INTERP", "0")):
        inplace_interpolate(filename)
    return
