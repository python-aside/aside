from _common import *
from _glyphs import glyph_to_path
from make_icon import make_icon


def make_logo(
    d: svgwrite.Drawing,
) -> Generator[Element, None, None]:
    yield from make_icon(d)

    text = []
    pos = list(text_start)
    for idx, letter in enumerate(logo_text[1:]):
        letter, w = glyph_to_path(d, logo_glyph_file.format(letter), text_size)
        if idx:
            pos[0] += w / 2
        letter.translate(pos)
        text.append(letter)
        pos[0] += w / 2
    d["width"] = pos[0]
    text = add_elements(d.g(), *text)
    yield from make_hi_body_lo(
        d,
        text,
        offset=(shadow_size, shadow_size),
        style=text_style,
        lo_style=shadow_style,
        hi=False,
    )


@commands.command("make_logo")
@click.argument("filename")
@click.option("--pretty", is_flag=True)
def main(filename: str, pretty: bool) -> Optional[int]:
    d = svgwrite.Drawing(size=(base_size, base_size))
    add_elements(d, *make_logo(d))
    d.saveas(filename, pretty=pretty)
    if not int(os.environ.get("INTERP", "0")):
        inplace_interpolate(filename)
    return
