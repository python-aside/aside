#!/bin/sh
. "$(dirname "${0}")/../_common.sh"

if [ -z "${SKIP_BOOTSTRAP+x}" ]; then
    scripts/venv/bootstrap.sh
fi

# Enforce locale-independent sorting order
export LC_COLLATE=C

# region Sort requirements/*.in
section_start sort "Sorting requirements/*.in files"

for req in "requirements/"*".in"; do
    sort "${req}" --output "${req}"
done

section_end sort
# endregion

# region Compile requirements/_txt
section_start compile "Compiling requirements/_txt"

export CUSTOM_COMPILE_COMMAND=scripts/venv/requirements_compile.sh
run_quiet "trying to compile requirements/_txt" \
    "${SYSTEM_PYTHON}" -m piptools compile \
    --allow-unsafe \
    --annotate \
    --build-isolation \
    --verbose \
    "setup.cfg" \
    "requirements/"*".in" \
    --output-file "requirements/_txt" \
    "${@}"

section_end compile
# endregion
