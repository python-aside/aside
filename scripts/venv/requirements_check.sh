#!/bin/sh
. "$(dirname "${0}")/../_common.sh"

# Enforce locale-independent sorting order
export LC_COLLATE=C

check_sorted() {
    if ! sort -C "${1}" >/dev/null; then
        echo "${1} is not sorted."
        echo "Did you forget to run \`venv/requirements_compile.sh\` after changing the requirements?"
        exit 1
    else
        echo "${1} is properly sorted."
    fi
}

# region Check requirements/*.in
section_start req_in "Checking requirements/*.in"

for req in "requirements/"*".in"; do
    run_quiet "check if ${req} are sorted" \
        check_sorted "${req}"
done

section_end req_in
# endregion

# ToDo: replace this with `piptools check` or `compile --check` if/when
#       [this issue](https://github.com/jazzband/pip-tools/issues/882) gets resolved.

# region Save current requirements/_txt
section_start save_req_txt "Saving current requirements/_txt"

TEMP="$(mktemp -u)"
cp "requirements/_txt" "${TEMP}"

section_end save_req_txt
# endregion

scripts/venv/requirements_compile.sh

check_same() {
    if ! diff "${1}" "${2}" >/dev/null; then
        mv "${2}" "requirements/_txt"
        echo "The requirements described in"
        echo "    setup.cfg"
        for req in "requirements/"*".in"; do
            echo "    ${req}"
        done
        echo "and"
        echo "    ${1}"
        echo "are out of sync."
        echo
        echo "Did you forget to run \`venv/requirements_compile.sh\` after changing the requirements?"
        exit 1
    else
        echo "${1} is up-to-date."
        rm "${TEMP}"
    fi
}

# region Check requirements/_txt
section_start check_req_txt "Checking current requirements/_txt"

run_quiet "check if requirements/_txt is up-to-date" \
    check_same "requirements/_txt" "${TEMP}"

section_end check_req_txt
# endregion
