#!/bin/sh
. "$(dirname "${0}")/../_common.sh"

PYTHON="${PYTHON:-${SYSTEM_PYTHON}}"
if [ -n "${VIRTUAL_ENV+x}" ]; then
    ALTINSTALL=""
    USER=""
else
    ALTINSTALL="--altinstall"
    USER="--user"
    export ENSUREPIP_OPTIONS=altinstall
fi

# region Bootstrap pip
section_start bootstrap_pip "Bootstrapping pip for ${PYTHON}"

run_quiet "ensure, that pip is installed" \
    "${PYTHON}" -m ensurepip \
    ${ALTINSTALL:+"${ALTINSTALL}"} \
    ${USER:+"${USER}"}

section_end bootstrap_pip
# endregion

# region Bootstrap meta packages
section_start bootstrap_meta "Bootstrapping meta packages for ${PYTHON}"

META_VERSIONS=""
while IFS= read -r TARGET; do
    VERSION=$(sed "s/^${TARGET}==\([0-9.]*\)$/\\1/p" --quiet <requirements/_txt)
    META_VERSIONS="${META_VERSIONS} ${TARGET}==${VERSION}"
done <requirements/meta.in

# shellcheck disable=SC2086
run_quiet "install meta packages" \
    "${PYTHON}" -m pip install \
    ${USER:+"${USER}"} \
    ${META_VERSIONS}

section_end bootstrap_meta
# endregion
