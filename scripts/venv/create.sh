#!/bin/sh
. "$(dirname "${0}")/../_common.sh"

if [ ! -f "requirements/_txt" ]; then
    echo "Requirements should be compiled before attempting to update a venv:"
    echo
    echo "scripts/venv/requirements_compile.sh"
    exit 1
fi

# region Create virtual environment
section_start create "Creating virtual environment"
${SYSTEM_PYTHON} -m venv .venv
section_end create
# endregion

export FORCE_SWITCH_VENV=1
activate_venv
scripts/venv/bootstrap.sh

# region Synchronize virtual environment requirements
section_start sync "Synchronizing virtual environment requirements"

run_quiet "sync the virtual environment with requirements/_txt" \
    "${PYTHON}" -m piptools sync \
    "requirements/_txt"

section_end sync
# endregion
