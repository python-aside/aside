"""Contains common boilerplate/repetitive code."""

from . import attributes, hooks, observable, timeutils
from .attributes import attrib, attrs

__all__ = [
    "attributes",
    "hooks",
    "observable",
    "timeutils",
    "attrs",
    "attrib",
]
