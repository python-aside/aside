"""Module entrypoint."""

if __name__ == "__main__":  # pragma: no cover
    from . import main

    main()
