from setuptools import setup

if __name__ == "__main__":
    # The setup of this package is done declaratively, via setup.cfg
    setup()
