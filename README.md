# aside

## Badges

<table style="margin-left: auto; margin-right: auto;"><tr>
<th align='right'>General:</th>
<td style='padding-left: 1em' align='center'><a href="https://pypi.org/project/aside"><img src="https://img.shields.io/pypi/v/aside?color=brightgreen&label=pypi%20%20latest" alt="pypi"/></a></td>
<td style='padding-left: 1em' align='center'><a href="https://gitlab.com/python-aside/aside/pipelines/master/latest"><img src="https://gitlab.com/python-aside/aside/badges/master/pipeline.svg" alt="pipeline"/></a></td>
</tr><tr>
<th align='right'>Style:</th>
<td style='padding-left: 1em' align='center'><img src="https://gitlab.com/python-aside/aside/-/jobs/artifacts/master/raw/.out/isort.svg?job=lint" alt="isort"/></td>
<td style='padding-left: 1em' align='center'><img src="https://gitlab.com/python-aside/aside/-/jobs/artifacts/master/raw/.out/black.svg?job=lint" alt="black"/></td>
</tr><tr>
<th align='right'>Docs:</th>
<td style='padding-left: 1em' align='center'><a href="https://aside.rtfd.io/en/stable"><img src="https://img.shields.io/readthedocs/aside/stable?label=%E2%A0%80%20stable%20%E2%A0%80" alt="stable"/></a></td>
<td style='padding-left: 1em' align='center'><a href="https://aside.rtfd.io/en/latest"><img src="https://img.shields.io/readthedocs/aside/latest?label=%E2%A0%80%20latest%20%E2%A0%80" alt="latest"/></a></td>
</tr><tr>
<th align='right'>Lint:</th>
<td style='padding-left: 1em' align='center'><img src="https://gitlab.com/python-aside/aside/-/jobs/artifacts/master/raw/.out/pylint.svg?job=lint" alt="pylint"/></td>
<td style='padding-left: 1em' align='center'><a href="https://python-aside.gitlab.io/"><img src="https://gitlab.com/python-aside/aside/-/jobs/artifacts/master/raw/.out/pydocstyle.svg?job=lint" alt="pydocstyle"/></a></td>
</tr><tr>
<th align='right'>Test:</th>
<td style='padding-left: 1em' align='center'><img src="https://gitlab.com/python-aside/aside/-/jobs/artifacts/master/raw/.out/pytest.svg?job=test" alt="pytest"/></td>
<td style='padding-left: 1em' align='center'><a href="https://gitlab.com/python-aside/aside/-/graphs/master/charts"><img src="https://gitlab.com/python-aside/aside/-/jobs/artifacts/master/raw/.out/coverage.svg?job=test" alt="coverage"/></a></td>
</tr></table>
<br/>

## Contributing

For more information about contributing to this project, see our
[contributing guidelines](https://aside.rtfd.io/en/latest/contributing).
